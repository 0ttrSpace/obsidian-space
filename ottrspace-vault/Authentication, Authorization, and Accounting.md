---
tags: 🌿️
aliases:
 - CyberSec
 - Networking
 - Security+ 601
created: 2022-03-05 1854
updated: 2022-03-07 0959
status:  🟧️️ 
---
[[Security+ 601]]

AAA is a framework for intelligently controlling access to computer resources, enforcing policies, auditing usage, and providing the information needed to bill for services. These are important for effective network management and security. 

#### Authentication
Authentication provides a way of identifying a user, each one with a unique set of criteria for  gaining access. 

Authentication enables organizations to keep their networks secure by permitting only authenticated users or processes to gain access to their protected resources. This can include computer systems, networks, databases, websites, and other network-based applications or services. 

Authentication is accomplished by the systems or resources being accessed. Depending on the use case for which authentications is used, authentication can consist of [[Singlefactor Authentication]], [[Multifactor Authentication]], or [[2 Factor Authentification]]. 

[[Strong Authentication]] is a term that is typically used to describe a type of authentication that is more reliable and resistant to attacks. Currently used authentication factor include: 

- Knowledge factor. The knowledge factor, or something you know, may be any authentication credentials that consist of information that the user possesses, including a personal identification number (PIN), a username, a password or the answer to a secret question.

- Possession factor. The possession factor, or something you have, may be any credential based on items that the user can own and carry with them, including hardware devices, like a security token or a mobile phone used to accept a text message or to run an authentication app that can generate a [[one-time password]] (OTP) or [[Personal Identification Number]].

- Inherence factor. The inherence factor, or something you are, is typically based on some form of biometric identification, including fingerprints or thumbprints, facial recognition, retina scan or any other form of biometric data.

- Location factor. Where you are may be less specific, but the location factor is sometimes used as an adjunct to the other factors. Location can be determined to reasonable accuracy by devices equipped with the Global Positioning System or with less accuracy by checking network addresses and routes. The location factor cannot usually stand on its own for authentication, but it can supplement the other factors by providing a means of ruling out some requests. For example, it can prevent an attacker located in a remote geographical area from posing as a user who normally logs in only from their home or office in the organization's home country.

- Time factor. Like the location factor, the time factor, or when you are authenticating, is not sufficient on its own, but it can be a supplemental mechanism for weeding out attackers who attempt to access a resource at a time when that resource is not available to the authorized user. It may also be used together with location. For example, if the user was last authenticated at noon in the U.S., an attempt to authenticate from Asia one hour later would be rejected based on the combination of time and location.

#### Authorization 
Authorization is the process of enforcing policies: determining what types or qualities of activities, resources, or services a user is permitted. 

Authorization includes the process through which an administrator grants rights to authenticated users, as well as the process of checking user account permissions to verify that the user has been granted access to those resources. The privileges and preferences granted for an authorized account depend on the user's permissions, which are either stored locally or on an authentication server. The settings defined for all these environment variables are established by an administrator.

#### Accounting
Accounting measures the resources a user can consume during access. This can include the amount of system time, the amount of data a user sent/received in a session. Account is managed by session logging statistics and usage information. These logs are also used for authorization control, billing, trend analysis, resource utilization, and capacity planning activities. 

AAA are often provided by a dedicated AAA Server, a program that performs these functions. The current standard by which network access servers interface with the AAA server is the [[Remote Authentication Dial-In User Service]]

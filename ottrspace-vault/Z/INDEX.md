---
tags: 🗺/✅/🟨
aliases: 
  - Home
cssclass:
  - tiny_img
created: 2022-03-16 1946
updated: 2022-03-26 2101
---

# Welcome To My Mind

My name is Ryan '0ttrSpace' Kohalmy. I am a Hacker who is still young in knowledge but there is no such thing as a Cyber Security expert. This Obsidian Vault is here to help me keep my ADHD brain in check and help me store thoughts, ideas, and information in a way that helps me stay organized and available for easy consumption later. 

My main passions in life are the topics you find the most common in this mess of information. These topics include: [[CyberSec]], [[OSINT]], [[Programing]], [[Linux]], [[ADHD]], [[Apple]], and [[Information Technology]]. Computers, I know. 

---
If you are looking through this as a look into Obsidian and you like the look of my setup there are a few things you should be aware of. 

To manage all notes in their various stages of processing I use this General utility notes that are tagged with #⚙️  such as this [[💳️ Kanban Live]]. I also keep track of input processing and daily notes with ISO 8601 date stamps in the titles of the notes which are laid out chronologically here: [[🕓️ Timeline]].

---

I also use the following notes along with the native [[Obsidian]] query syntax to collect lists of my [[Zettelkasten]] inputs by type:

[[{]] *<=>* `Books`
[[@]] *<=>* `People`
[[!]] *<=>* `Tweets`
[[%]] *<=>* `Podcasts`
[[+]] *<=>* `YouTube`
[[(]] *<=>* `Articles`
[[&]] *<=>* `Papers`
[[=]] *<=>* `Thoughts`

To see the input by status, each has status tags denoted with a colored square that is elaborated on more in [[Tag Taxonomy]]. 

I also manage my [[TODO]]'s in much the same way with the colored squares except in this instance the squares indicate level of difficulty / involvement to complete the task.

To really explore and jump off into this vault you should start with the [[Map Of Content|Maps of Content]] which are all found easily with #🗺 The Map of MOC is gone because it's more effort than it's worth to maintain and the MOC's are becoming more free form and connecting to each other and there's no longer a point in trying to code a structure around a free form network graph.

---

#### GLHF
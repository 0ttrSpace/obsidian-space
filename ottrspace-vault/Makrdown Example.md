---
created: 2022-03-27 1516
updated: 2022-03-27 1519
---
# H1

## H2

### H3

#### H4

##### H5

###### H6

## Blockquotes
> Don’t communicate by sharing memory, share memory by communicating.
> 
> — Rob Pike[1](https://hugo-toha.github.io/posts/markdown-sample/#fn:1)

## Tables
| Name  | Age |
| ----- | --- |
| Bob   | 34  |
| Alice | 41  |

## [Code Blocks](https://hugo-toha.github.io/posts/markdown-sample/#code-blocks)
```js
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```
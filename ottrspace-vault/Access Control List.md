---
tags: 🧠️/📝️/🌲️ ✅/🟨
publish: true
aliases: 
 - Access Control List
 - CyberSec
 - Security+ 601
created: 2022-03-06 0955
updated: 2022-03-26 2103
status: 🟩
---

# [[Access Control List]]

---

<% tp.file.cursor(2) %>

---

- Tags: 
	- [[Security+ 601]] 
	- [[CyberSec]] 
	- [[Networking]] 
- Reference:
	- 
- Related:
	-  

Access Control Lists are a common and useful security tool that allows us to deny unwanted access to a network while allowing internal users appropriate access to services. ACL are a set of allow/deny commands, grouped (generally by name or number), that's used to filter traffic entering or leaving and interface

These group statements define wether packets are accepted or rejected coming into or leaving an interface. If one condition match is evaluated true the packet is either accepted or rejected and the other statements are ignored. Because of the sequential order working of ACL an implicit 'deny any' statement is placed at the end of an Access Control List by default. 

- Numbered and named Access Control Lists: Numbered ACL is assigned a unique number among all ACL, but a named is identified by a unique name. 
- Standard and Extended Access Control List: Standard IP Access Control List can be used to filter traffic based solely on the source IP address of the IP datagram packet. An extended ACL can be used to filter ftraffic based on the source IP address, Destination IP address, Protocol (TCP, UDP, etc), Port number, etc. 

| Access Control List Type | Access Control Lists Numbers |
| ------------------------ | ---------------------------- |
| IP Standard              | 1-99, 1300-1999              |
| IP Extended              | 100-199, 2000-2699           |


